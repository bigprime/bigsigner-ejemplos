import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArchivosComponent } from './archivos/archivos.component';

const routes: Routes = [
      {path: 'archivos', component: ArchivosComponent},
      {path: '', redirectTo:'archivos', pathMatch:'full'},
      {path: '**', redirectTo:'archivos', pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
