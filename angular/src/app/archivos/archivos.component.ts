import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { map, filter, switchMap } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';

import {saveAs} from 'file-saver';
import * as JSZip from 'jszip';
import * as FileSaver from 'file-saver';

@Component({
  selector: 'app-archivos',
  templateUrl: './archivos.component.html',
  styleUrls: ['./archivos.component.css']
})
export class ArchivosComponent implements OnInit {

  filesUpload;
   zip = new JSZip();
  formData: FormData = new FormData();
  public param: any = {
    code: null
  };
  public param1: any ={
    code:null
  }
  public paramDownload: any = {
    processCode: null,
    documentId: null
  }
  
  fileUrl;
  //options = {'Content-Type': 'multipart/form-data'} as any;
  constructor(private _http: HttpClient, private sanitizer: DomSanitizer) { }

  ngOnInit() {
  }
  uploadFile(files: FileList) {

    //  var formData = new FormData();
  
    let file: File = files[0];
    
    /*for(let i =0; i < files.length; i++){
this.formData.append("files", files[i], files[i]['name']);
 }*/
    let formData: FormData = new FormData();
    formData.append('uploadFile', file, file.name);
    let headers = new HttpHeaders();
    /** In Angular 5, including the header Content-Type can invalidate your request */
    headers.append('enctype', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    let options = {
      headers: headers
    }
    this._http.post('http://digital-signature-client-side.us-east-1.elasticbeanstalk.com/api/signfile', formData, options).subscribe((response) => {
      console.log('response', response)
        console.log('code del file', response["code"]);

        this.param.code = response["code"];
        console.log(this.param.code);
      } )
  }

  consultarEstado() {
    this._http.post('http://digital-signature-client-side.us-east-1.elasticbeanstalk.com/api/getstatus', this.param)
      .subscribe((response) => {
        console.log('response', response)
        this.paramDownload.processCode = response["code"];
         response["documentFileMapperList"].forEach(element => {
          this.paramDownload.documentId=element.id
        })
      },
        (error) => {
          console.log('error in status', error)
        })

  }

  descargarPDFFirmado() {

    const pdfUrl = './assets/pdf';
    const pdfName = 'documento';


    this.downloadReportService().subscribe(
      data => {
        const blob = new Blob([data], { type: "application/pdf"});
        console.log(data);
        saveAs(blob, pdfName);
     
      },
      err => {
        alert("Problem while downloading the file.");
        console.error(err);
      }
    );

  }


  public downloadReportService(): Observable<any> {
    // Create url
    let url = 'http://digital-signature-client-side.us-east-1.elasticbeanstalk.com/api/getsigneddocument';
    return this._http.post(url, this.paramDownload, {
      responseType: "blob",
      headers: new HttpHeaders().append("Content-Type", "application/json")
    });
  }

  //metodo que sube varios files
  uploadFiles(files: FileList){

    
    console.log(files);
  
    for (let i = 0; i < files.length; i++) {
      this.filesUpload = files;
    }
    console.log(this.filesUpload);
    
    this.generarZip(this.filesUpload);

  }

  //subir zip (generado a partir de los files) para firmar
  generarZip(filesUpload){
    
    for (let i = 0; i < filesUpload.length; i++) {
      this.zip.file(filesUpload[i].name, filesUpload[i]);

    }
    
    
      //metodo para generar zip
      this.zip.generateAsync({ type: "blob" })
      .then( (content) => {
        if (content != undefined) {
          console.log(content);
      //metodo para descargar .zip
        FileSaver.saveAs(content, "Docs.zip");
        console.log(content);
        this.uploadZipParaFirmar(content);
        }
      });

  }

  //enviar .zip a la api de firma en lotes
  uploadZipParaFirmar(zipcontent){

    console.log(zipcontent);
    
    let formData: FormData = new FormData();
    formData.append('fileType', zipcontent , "zip.zip");
    let headers = new HttpHeaders();
    
   headers.append('enctype', 'multipart/form-data');
    headers.append('Accept', 'application/json');
    let options = {
      headers: headers
    }
    this._http.post('http://digital-signature-client-side.us-east-1.elasticbeanstalk.com/api/signbatch', formData, options)
      .subscribe((response) => {
      console.log('response', response);
      console.log(response["code"]);
      
      this.param1.code = response["code"];
      } )

  }

  /////para la descarga del zip

  descargarZIPFirmado(){
    const zipName = 'zipDocumentosFirmados';


    this.descargarZIPFirmadoService().subscribe(
      data => {
        const blob = new Blob([data], { type: "application/zip"});
        console.log(data);
        saveAs(blob, zipName);
     
      },
      err => {
        alert("Problem while downloading the zip.");
        console.error(err);
      }
    );
  }
  descargarZIPFirmadoService(): Observable<any>{

    let url = 'http://digital-signature-client-side.us-east-1.elasticbeanstalk.com/api/zipsigneddocument';
    return this._http.post(url, this.param1, {
      responseType: "blob",
      headers: new HttpHeaders().append("Content-Type", "application/json")
    });
  }

}
