<?php
/**
 * Created by PhpStorm.
 * User: lhinostroza
 * Date: 23/09/2019
 * Time: 15:30
 */


// This is the entire file that was uploaded to a temp location.
$localFile = "/Users/luishinostrozasaldana/Documents/ARCHIVOS_DEMO/11111111.pdf";


// Connecting to website.
$ch = curl_init("http://localhost:9001/api/signfile");

if (function_exists('curl_file_create')) { // php 5.5+
    $cFile = curl_file_create($localFile);
} else { //
    $cFile = '@' . realpath($localFile);
}
$post = array('file_contents' => $cFile);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

$result = curl_exec($ch);
curl_close($ch);

echo "\n";
print_r($result);

$json_decode = json_decode($result, true);
echo "\n";
echo $json_decode["signUrl"];
echo "\n";
echo $json_decode["code"];