import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;


public class UploadFileToRestAPIClientSign {
    public static void main(String[] args) throws Exception {


        URL url = new URL("PATH_TO_API_SERVICE");
        File file = new File("PATH_PDF_OR_ZIP");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setDoOutput(true);
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-type", "application/pdf");

        final long documentLength = file.length();
        conn.setFixedLengthStreamingMode(documentLength);

        OutputStream os = conn.getOutputStream();
        try (InputStream documentIS = new BufferedInputStream(new FileInputStream(file))) {
            byte[] b = new byte[4096];

            int readCount;
            while (-1 != (readCount = documentIS.read(b))) {
                os.write(b, 0, readCount);
            }
        }

        int statusCode = conn.getResponseCode();

        String responseFromServer = "";
        if (statusCode == 200) {
            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            while ((output = br.readLine()) != null) {
                responseFromServer = output;
            }
        }
        conn.disconnect();

        System.out.println(statusCode);

        //Se imprime la respuesta del servidor
        System.out.println(responseFromServer);
    }
}
