﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
namespace clienteSign
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Firma Unitaria");
            //UploadFile();
            //status(new ResultUpload { code = "06a53cda980361b878cc338ce2aaeba993f3cc52a3e6eb0fec6ba0a9d07c60d0" });
            DownloadFiles(new ResultUpload { code = "06a53cda980361b878cc338ce2aaeba993f3cc52a3e6eb0fec6ba0a9d07c60d0", documentId = "3053" });
            Console.ReadKey();
        }

        private static async void UploadFile()
        {
            Console.WriteLine("Empezando");
            var client = new HttpClient();

            var ruta = @"C:/Users/LENOVO/Desktop/demo4.pdf";
            FileStream fs = null;
            try { fs = File.OpenRead(ruta); }
            catch (Exception e)
            {
                Console.WriteLine("El archivo no Existe!" + e);
                return;
            }

            var content = new ProgressStreamContent(fs, CancellationToken.None);
            content.Progress = (bytes, totalBytes, totalBytesExpected) =>
            {
                Console.WriteLine("Uploading {0}/{1} => {2}%", totalBytes, totalBytesExpected, Convert.ToInt32(((float)totalBytes / (float)totalBytesExpected) * 100));
            };
            var response = await client.PostAsync("http://localhost:9005/api/signfile?vis_sig_text=Firmado digitalmente por el Medico: <SIGNER> <br> MAS CONTENIDO PARA EL CARGO ETC NEW BIGSIGNER 2 C/S FONDO<br>Fecha: <DATE>&vis_sig_width=115&vis_sig_height=60", content);

            if (response.IsSuccessStatusCode)
                {
                    var resultUpload = JsonConvert.DeserializeObject<ResultUpload>(await response.Content.ReadAsStringAsync());
                    Console.WriteLine($"signUrl: {resultUpload.signUrl}");
                    Console.WriteLine($"code: {resultUpload.code}");
                    Console.WriteLine($"status: {resultUpload.status}");
                    Console.WriteLine($"path: {resultUpload.pathDownloadSignDocument}");
                }
            
        }

        private static async void status(ResultUpload f)
        {
            Console.WriteLine("estado del Archivo Firmado");
            var client = new HttpClient();          
            var json = JsonConvert.SerializeObject(f);
            var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

            var response = await client.PostAsync("http://localhost:9005/api/getstatus", content);

            if (response.IsSuccessStatusCode)
            {
                var resultUpload = JsonConvert.DeserializeObject<ResultUpload>(await response.Content.ReadAsStringAsync());
                Console.WriteLine($"signUrl: {resultUpload.signUrl}");
                Console.WriteLine($"code: {resultUpload.code}");
                Console.WriteLine($"status: {resultUpload.status}");
                Console.WriteLine($"documentFileMapperList: {resultUpload.documentFileMapperList.Count}");
                foreach (var item in resultUpload.documentFileMapperList)
                {
                    Console.WriteLine("id: {0}, name: {1}", item.id, item.documentName);
                }
            }
        }

        private static async void DownloadFiles(ResultUpload f)
        {
            Console.WriteLine("Descargar Archivo Firmado");
            var client = new HttpClient();
            var json = JsonConvert.SerializeObject(f);
            var content = new StringContent(json, System.Text.Encoding.UTF8, "application/json");

            var response = await client.PostAsync("http://localhost:9005/api/getsigneddocument", content);

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("Devolviendo archivo en Archivo Firmado ");
                var b = await response.Content.ReadAsByteArrayAsync();
                String path = @"C:/Users/LENOVO/Desktop/demo4firmado.pdf";//se guarda con la extension requerida!
                System.IO.File.WriteAllBytes(path, b);
                Console.WriteLine("el archivo se guardo en :" + path);

            }
            var j = JsonConvert.DeserializeObject<ResultUpload>(json);
            Console.WriteLine("Finish -> Code : " + j.code + " DocumentId :" + j.documentId);
        }
        public class ResultUpload
        {
            public string signUrl { get; set; }
            public string code { get; set; }
            public int status { get; set; }
            public string documentId { get; set; }

            public List<DocumentFileMapperList> documentFileMapperList { get; set; }
            public string pathDownloadSignDocument { get; set; }
        }

        public class DocumentFileMapperList
        {
            public string id { get; set; }
            public string documentName { get; set; }
            public string  signDate { get; set; }
           
        }

        public delegate void ProgressDelegate(long bytes, long totalBytes, long totalBytesExpected);

        public class ProgressStreamContent : StreamContent
        {
            public ProgressStreamContent(Stream stream, CancellationToken token)
                : this(new ProgressStream(stream, token))
            {
            }

            public ProgressStreamContent(Stream stream, int bufferSize)
                : this(new ProgressStream(stream, CancellationToken.None), bufferSize)
            {
            }

            ProgressStreamContent(ProgressStream stream)
                : base(stream)
            {
                init(stream);
            }

            ProgressStreamContent(ProgressStream stream, int bufferSize)
                : base(stream, bufferSize)
            {
                init(stream);
            }

            void init(ProgressStream stream)
            {
                stream.ReadCallback = readBytes;

                Progress = delegate { };
            }

            void reset()
            {
                _totalBytes = 0L;
            }

            long _totalBytes;
            long _totalBytesExpected = -1;

            void readBytes(long bytes)
            {
                if (_totalBytesExpected == -1)
                    _totalBytesExpected = Headers.ContentLength ?? -1;

                long computedLength;
                if (_totalBytesExpected == -1 && TryComputeLength(out computedLength))
                    _totalBytesExpected = computedLength == 0 ? -1 : computedLength;

                // If less than zero still then change to -1
                _totalBytesExpected = Math.Max(-1, _totalBytesExpected);
                _totalBytes += bytes;

                Progress(bytes, _totalBytes, _totalBytesExpected);
            }

            ProgressDelegate _progress;
            public ProgressDelegate Progress
            {
                get { return _progress; }
                set
                {
                    if (value == null) _progress = delegate { };
                    else _progress = value;
                }
            }

            protected override Task SerializeToStreamAsync(Stream stream, TransportContext context)
            {
                reset();
                return base.SerializeToStreamAsync(stream, context);
            }

            protected override bool TryComputeLength(out long length)
            {
                var result = base.TryComputeLength(out length);
                _totalBytesExpected = length;
                return result;
            }

            class ProgressStream : Stream
            {
                CancellationToken token;

                public ProgressStream(Stream stream, CancellationToken token)
                {
                    ParentStream = stream;
                    this.token = token;

                    ReadCallback = delegate { };
                    WriteCallback = delegate { };
                }

                public Action<long> ReadCallback { get; set; }

                public Action<long> WriteCallback { get; set; }

                public Stream ParentStream { get; private set; }

                public override bool CanRead { get { return ParentStream.CanRead; } }

                public override bool CanSeek { get { return ParentStream.CanSeek; } }

                public override bool CanWrite { get { return ParentStream.CanWrite; } }

                public override bool CanTimeout { get { return ParentStream.CanTimeout; } }

                public override long Length { get { return ParentStream.Length; } }

                public override void Flush()
                {
                    ParentStream.Flush();
                }

                public override long Position
                {
                    get { return ParentStream.Position; }
                    set { ParentStream.Position = value; }
                }

                public override int Read(byte[] buffer, int offset, int count)
                {
                    token.ThrowIfCancellationRequested();

                    var readCount = ParentStream.Read(buffer, offset, count);
                    ReadCallback(readCount);
                    return readCount;
                }

                public override long Seek(long offset, SeekOrigin origin)
                {
                    token.ThrowIfCancellationRequested();
                    return ParentStream.Seek(offset, origin);
                }

                public override void SetLength(long value)
                {
                    token.ThrowIfCancellationRequested();
                    ParentStream.SetLength(value);
                }

                public override void Write(byte[] buffer, int offset, int count)
                {
                    token.ThrowIfCancellationRequested();
                    ParentStream.Write(buffer, offset, count);
                    WriteCallback(count);
                }

                protected override void Dispose(bool disposing)
                {
                    if (disposing)
                    {
                        ParentStream.Dispose();
                    }
                }
            }
        }
    }
}